const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const authRoute = require('../src/routes/auth')
const mainRoute = require('../src/routes/main')
const userRoute = require('../src/routes/user')
const productRoute = require('../src/routes/product')

const port = 5900

app.set("view engine", "ejs");
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.use(express.static(__dirname + '/public'));
app.use(express.static('/resources/images'));

app.use('/', authRoute)
app.use('/me', userRoute)
app.use('/mainpage', mainRoute)
app.use('/products', productRoute)

if (typeof localStorage === "undefined" || localStorage === null) {
    const LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

app.listen(port, () => {
    console.log("zdarova on port ${port}.")
})