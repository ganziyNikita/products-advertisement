const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')


router.get('/', function (req, res) {
    console.log("HEADERCONTAIN->"+req.headers.get('token'))
    res.render('mainpage.ejs')
})

router.get('/me', function (req, res) {
    res.redirect('/me')
})

router.get('/products', function (req, res) {
    res.redirect('/products')
})


module.exports = router