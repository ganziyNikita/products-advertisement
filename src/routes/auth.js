const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')


router.get('/', function (req, res) {
    res.redirect('/login')
})

router.post('/login', async function (req, res) {
    const token = await userController.login(req, res)
    if (token) {
        res.setHeader('token',token)
        console.log('TESTSSSSS'+res.getHeader('token'))
        res.redirect('/mainpage')
    }
})

router.post('/reg', function (req, res) {
    userController.registration(req, res)
})

router.get('/reg', function (req, res) {
    res.render('registration.ejs')
})

router.get('/login', function (req, res) {
    res.render('login.ejs')
    response = res;
})


module.exports = router