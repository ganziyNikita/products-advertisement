const models = require('../models')
const jwt = require('jsonwebtoken')

exports.createProduct = async function (product, errors, id) {
    await models.Product.create({
        name: product.name,
        description: product.description,
        price: product.price,
        userId: id

    }).catch(err => errors = err)
}

exports.deleteProduct = async function (productId,errors)
{
    const product=models.product.findOne({where:{id:productId}})

    if(!product){
        errors="Product not found"
    }
    else{
        product.destroy()
    }
}

exports.getAllProducts = async function(userId,errors)
{
    const listOfProducts=await models.Product.findAll({where:{Userid:userId}})
    return listOfProducts
}