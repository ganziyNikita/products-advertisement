const models = require('../models')
const jwt = require('jsonwebtoken')


exports.getUser = async function (email) {
    //todo
}

exports.register = async function (user, errors) {

    const createdUser = await models.User.create({
        login: user.login,
        password: user.password,
        firstName: user.firstName,
        lastName: user.lastName,
        age: user.age,
        email: user.email,
    }).catch(err => errors = err)
    if (errors) {
        return;
    }
    else if (!user) {
        errors = "creating is canceled"
    }
    else {
        localStorage.setItem('accessToken', generateAccessToken(createdUser.id));
    }

}
exports.updateUser = async function(user, userId,errors){
    await models.User.update({
        login: user.login,
        password: user.password,
        firstName: user.firstName,
        lastName: user.lastName,
        age: user.age,
        email: user.email,
    },
    {
        where: {id:userId}
    }).catch(err=>errors=err)
}
exports.login = async function (login, password, errors) {
    const user = await models.User.findOne({
        where: {
            login: login
        }
    })
    if (!user) {
        errors = "user not found"
    }
    if (user.password != password) {
        errors = "incorrect password"
    }
    return localStorage.getItem('accessToken');
}

function generateAccessToken(id) {
    const payload = {
        sub: id,
        JTI: "d35fdsk34fksd9i435t",
        iss: "server localhost",
    }
    //пофиксить env shobi peremennoy key use
    const accessToken = "929e20daa3a8f522a75236ef17cd77d55e35d55d41c16382d43fa8f922ecf27145dd70b2bcff41dcefe684f9344498b468548158ebcad2f10ad5dcb9f41fbfaa"

    return jwt.sign(payload, accessToken,
        { expiresIn: '3d' })
}

exports.verifyAccessToken = async function () {
    const token = localStorage.getItem('accessToken');
    const decoded = jwt.verify(token, "929e20daa3a8f522a75236ef17cd77d55e35d55d41c16382d43fa8f922ecf27145dd70b2bcff41dcefe684f9344498b468548158ebcad2f10ad5dcb9f41fbfaa")


    const user = models.User.findByPk(decoded.sub)
    if (!user) {
        return false;
    }
    return true;
}
