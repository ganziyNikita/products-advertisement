const userService = require('../services/user')

exports.registration = function (req, res) {

    const errors = "";
    const user = {
        login: req.body.login,
        password: req.body.password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        age: req.body.age
    }
    userService.register(user, errors)
    if (!errors) {
        res.status(400).json({ error: errors })
    }
    //else
    //res.redirect('/auth/login')
}

exports.login = async function (req, res) {

    const errors = "";
    const token = await userService.login(req.body.login, req.body.password, errors)
    if (errors.length > 0) {
        return token;
    }
    return token;
}

exports.updateUser = async function (req, res, id) {
    const errors=""
    const user = {
        login: req.body.login,
        password: req.body.password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        age: req.body.age
    }
    userService.updateUser(user,id,errors)
    //if(errors.length>0)
}