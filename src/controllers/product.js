const productService = require('../services/product')

exports.createProduct = function (product, userId) {
    //todo
    const errors=""
    productService.createProduct(product,errors,userId)
}

exports.getAllProducts = function (userId) {
    //todo
    const errors=""
    const listOfProduct=productService.getAllProducts(userId)
    return listOfProduct
}

exports.deleteProduct = function (productId)
{
    const errors=""
    productService.deleteProduct(productId,errors)
}
